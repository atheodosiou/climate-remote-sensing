clc
clear all
close

% Script to convert the Jiangjun's file with patch geometry
% into the mat-files

path_in = 'orig/';
n_patches = 23      % Number of patches

poly_cell_ais400 = cell(n_patches,1);
% Reading patch geometry
for k_patch=1:n_patches
  patch = sprintf('%d',k_patch);
  disp(['Reading the patch: ' patch])
  file_in = [path_in 'patch' patch '_new_300000.000000000m_block.txt'];
  fid = fopen(file_in,'rt');
  contents = textscanh(fid);
  fclose(fid);
  lon = contents(:,1);
  lat = contents(:,2);
  [p_lon p_lat] = poly2cw(lon,lat); % Forcing the clockwise orientation of polygons
  poly_cell_ais400(k_patch) = {[p_lon p_lat]};
end

% Storing the geometry of individual patches
save('poly_cell_greenland23.mat','poly_cell_ais400');

% visualization (Arctica)
coast = load('coast');
h = figure;axesm('stereo','Origin',[90 0 0],'FLatLimit',[-Inf 30]);
axis off; framem on; gridm on; mlabel off; plabel off;
%geoshow(lat,lon,val, 'DisplayType', 'texturemap');colorbar
hold on;
plotm(coast.lat,coast.long,'k');
for k_patch=1:n_patches
  lon = poly_cell_ais400{k_patch}(:,1);
  lat = poly_cell_ais400{k_patch}(:,2);
  plotm(lat,lon,'r');
end

% Computing a data area (that included a 300-km buffer zone around Greenland)
R = 6371;  % in km
b_width = 300;   % buffer zone width = 300 km
for k_patch=1:n_patches
  lon = poly_cell_ais400{k_patch}(:,1);
  lat = poly_cell_ais400{k_patch}(:,2);
  [b_lat,b_lon] = bufferm(lat,lon,b_width/R*180/pi,'outPlusInterior'); % Computing the individual buffer zones
  plotm(b_lat,b_lon,'b');
  if k_patch == 1
    m_lat = b_lat; m_lon = b_lon;
  else
    [m2_lon m2_lat] = polybool('union',b_lon,b_lat,m_lon,m_lat); % Merging the polygons
    m_lat = m2_lat; m_lon = m2_lon;
  end
end

poly_cell_obs = cell(1,1);
poly_cell_obs(1) = {[m_lon m_lat]};
save('poly_cell_obs_300km.mat','poly_cell_obs');

plotm(m_lat,m_lon,'g+');
