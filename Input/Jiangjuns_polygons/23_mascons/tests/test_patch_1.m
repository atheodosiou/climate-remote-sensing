% visualization (Arctica)
coast = load('coast');
h = figure;axesm('stereo','Origin',[90 0 0],'FLatLimit',[-Inf 30]);
axis off; framem on; gridm on; mlabel off; plabel off;
%geoshow(lat,lon,val, 'DisplayType', 'texturemap');colorbar
hold on;
plotm(coast.lat,coast.long,'k');

R = 6371;  % in km
b_width = 300;   % buffer zone width = 300 km
load('patch_1.mat','lon','lat');
plotm(lat,lon,'r');
[p_lat p_lon] = poly2cw(lat,lon); % Forcing the clockise orientation of polygons
[b_lat,b_lon] = bufferm(p_lat,p_lon,b_width/R*180/pi,'out'); % Computing the individual buffer zones
plotm(b_lat,b_lon,'b');
