function data = textscanh(fid,nlines)

%Reads numerical data from ASCII text file, ignoring header lines, which
%are considered so if hold non-numerical data. The file needs to be opened
%outside this routine and the corresponding <fid> inputted.
%
%The optional input <nlines> determines how many lines are read. If
%ommited, empty or -1, the whole file is returned.
%
%This function makes use of <textscan>, which determines automatically the
%number of columns and lines (if empty <nlines>) of the output <data>.
%
%example:
%
%fid = fopen(<some file>, 'r');
%a=textscanh(fid)
%fclose(fid);

if nargin < 2 || isempty(nlines)
    nlines = -1;
end
if nargin < 1 || isempty(fid)
    error([mfilename,': need one input file.'])
end
if ischar(fid)
    fid = fopen(fid,'r');
    need_to_close = true;
else
    need_to_close = false;
end

%maximum number of lines to scan for header
max_header_len = 50;

%init header
header = zeros(max_header_len,1);
%determining number of header lines
for i=1:max_header_len
    line = fgetl(fid);
    header(i) = isnumstr(line);
end
%checking for number of header lines > max_header_len
if ~any(header)
    error([mfilename,': file ',fopen(fid),' has more header lines than max search value (',num2str(max_header_len),').']);
end

header = find(diff(header)==1,1,'last');
if isempty(header)
    header = 0;
end

%disp([mfilename,':debug: header contains ',num2str(header),' lines.'])

frewind(fid);

data  = textscan(fid,'',nlines,'headerlines',header,...
                   'returnonerror',0,'emptyvalue',0);

%need to be sure that all columns have equal length
min_len = 1/eps;
for i=1:length(data)
    min_len = min(size(data{i},1),min_len);
end
%cropping so that is true
for i=1:length(data)
    data{i} = data{i}(1:min_len,:);
end
                
%numerical output, so transforming into numerical array
data=[data{:}];

if need_to_close
    fclose(fid);
end

function out = isnumstr(in)

%Determines if the input string holds only numerical data. The test that is
%made assumes that numerical data includes only the characters defined in
%<num_chars>.

%characters that are allowed in numerical strings
num_chars = ['1234567890.-+EeDd',9,10,13,32];

%deleting the numerical chars from <in>
for i=1:length(num_chars)
    num_idx = strfind(in,num_chars(i));
    if ~isempty(num_idx)
        %flaging this character
        in(num_idx)='';
    end
end

%now <in> must be empty (if only numerical data)
out = isempty(in);

%warning
%if ~out
%    disp([mfilename,':debug: non-numerical chars: ',in])
%end
