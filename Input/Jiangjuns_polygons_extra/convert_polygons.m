clc
clear all
close

% Script to convert the Jiangjun's file with patch geometry
% into the Olga's format

path_in = 'orig/';
n_patches = 9      % Number of patches
max_patch = 10     % Maxumum patch number

poly_cell_extra = cell(n_patches,1);
% Reading patch geometry
k = 0;
for k_patch=1:max_patch
  if k_patch ~= 4
    k = k+1;
    patch = sprintf('%d',k_patch);
    disp(['Reading the patch: ' patch])
    file_in = [path_in 'region_' patch '.txt'];
    fid = fopen(file_in,'rt');
    contents = textscanh(fid);
    fclose(fid);
    lon = contents(:,1);
    lat = contents(:,2);
    [p_lon p_lat] = poly2cw(lon,lat); % Forcing the clockise orientation of polygons
    poly_cell_extra(k) = {[p_lon p_lat]};
  end
end

% Storing the geometry of individual patches
save('poly_cell_extra.mat','poly_cell_extra');

% visualization (Arctica)
coast = load('coast');
h = figure;axesm('stereo','Origin',[90 0 0],'FLatLimit',[-Inf 45]);
axis off; framem on; gridm on; mlabel off; plabel off;
%geoshow(lat,lon,val, 'DisplayType', 'texturemap');colorbar
hold on;
plotm(coast.lat,coast.long,'k');
for k_patch=1:n_patches
  lon = poly_cell_extra{k_patch}(:,1);
  lat = poly_cell_extra{k_patch}(:,2);
  plotm(lat,lon,'r');
end
