clc
clear all
close

% Script to define the Greenland geometry (as a mat-file)
% as well as to define the data areas (Greenland territory
% extended with a 250-km or 300-km buffer zone)

file_in = 'orig/greenland_polygon.txt';

poly_cell_ais400 = cell(1,1);
fid = fopen(file_in,'rt');
contents = textscanh(fid);
fclose(fid);
lon = contents(:,1);
lat = contents(:,2);
np = size(lon,1)    % Number of points in the polygon
% lon(np+1) = lon(1); lat(np+1) = lat(1); % Closing the polygon is not needed here

[p_lon p_lat] = poly2cw(lon,lat); % Forcing the clockise orientation of polygon
poly_cell_ais400(1) = {[p_lon p_lat]};

% visualization (Arctica) and storage
coast = load('coast');
h = figure;axesm('stereo','Origin',[90 0 0],'FLatLimit',[-Inf 40]);
axis off; framem on; gridm on; mlabel off; plabel off;
%geoshow(lat,lon,val, 'DisplayType', 'texturemap');colorbar
hold on;
plotm(coast.lat,coast.long,'k');
lon = poly_cell_ais400{1}(:,1);
lat = poly_cell_ais400{1}(:,2);
plotm(lat,lon,'r');
save('poly_cell_ais400.mat','poly_cell_ais400');

% Computing a 250-km buffer zone around Greenland, visualization, and storage
R = 6371;  % in km
b_width = 250;   % buffer zone width = 250 km
[b_lat,b_lon] = bufferm(lat,lon,b_width/R*180/pi,'outPlusInterior'); % Computing the buffer zone
poly_cell_obs = cell(1,1);
poly_cell_obs(1) = {[b_lon b_lat]};
plotm(b_lat,b_lon,'b+');
save('poly_cell_obs_250km.mat','poly_cell_obs');

% Computing a 300-km buffer zone around Greenland, visualization, and storage
b_width = 300;   % buffer zone width = 300 km
[b_lat,b_lon] = bufferm(lat,lon,b_width/R*180/pi,'outPlusInterior'); % Computing the buffer zone
poly_cell_obs = cell(1,1);
poly_cell_obs(1) = {[b_lon b_lat]};
plotm(b_lat,b_lon,'g+');
save('poly_cell_obs_300km.mat','poly_cell_obs');
