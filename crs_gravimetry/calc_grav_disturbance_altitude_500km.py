#!/usr/bin/env python3
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

from gravity_disturbance import calc_grav_disturbance, setup_grav_computation

if __name__ == '__main__':
    lambda_p = np.pi/180 * np.array(range(0, 360 + 1,4))
    theta_p = np.pi/180 * np.array(range(1, 179 + 1,4))
    dclm_2003, dclm_2013, dslm_2003, dslm_2013, R, GM = setup_grav_computation()
    love_numbers_file = Path("../Input/load_love_numbers_96.txt")
    k_l = np.loadtxt(love_numbers_file, skiprows=0, usecols=1)
    ewh2003, ewh2013 = calc_grav_disturbance(dclm_2003, dclm_2013, dslm_2003, dslm_2013, k_l, theta_p, lambda_p, R, GM)
    # make plot

    # global plot

    latitude = np.pi/2 - theta_p
    # MAP = Basemap(projection='cyl', llcrnrlon=0., urcrnrlon=360., resolution='c', llcrnrlat=-90., urcrnrlat=90.)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())

    # draw coastlines
    # MAP.drawcoastlines()
    ax.coastlines()

    x, y = np.meshgrid(np.rad2deg(lambda_p), np.rad2deg(latitude))

    # make the plot
    cnplot = ax.pcolormesh(x, y, ewh2013-ewh2003)

    # make  the color bar
    clevs = np.arange(-3, 3, 1)
    cbar = fig.colorbar(cnplot, location='bottom')

    plt.xlabel('Longitude', labelpad=8, fontsize=8)
    plt.ylabel('Latitude', labelpad=8, fontsize=8)
    plt.title('Change in gravitational disturbance at H=500 km in 2003-2013 (m/s^2)', pad=14, fontsize=12)
    plt.savefig("../Output/delta_g_altitude-500.pdf")
    plt.show()
