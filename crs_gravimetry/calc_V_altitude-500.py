#!/usr/bin/env python3
import numpy as np
import re
# from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

# function to compute normalized Legendre function
def plm(theta, degree):
    p_lm = np.zeros((degree + 1, degree + 1))
    u = np.sqrt(1 - np.cos(theta)**2)
    for l in range(degree +1):
        if l == 0:
            p_lm[l, l] = 1
        elif l == 1:
            p_lm[l, l] = u * np.sqrt(3)
        elif l > 1:
            p_lm[l, l] = u * np.sqrt((2*l + 1) / (2*l)) * p_lm[l-1, l-1]

    for l in range(degree + 1):
        for m in range(0, l+1):
            if m == l-1:
                if m == 0:
                    delta = 1
                else:
                    delta = 0
                a_lm = (2/np.sqrt(1+delta)) * ((m+1)/np.sqrt((l-m)*(l+m+1)))
                p_lm[l, m] = a_lm * (np.cos(theta)/u) * p_lm[l, m+1]

    for l in range(degree + 1):
        for m in range(l-2, 0 - 1, -1):
            if m == 0:
                delta = 1
            else:
                delta = 0
            a_lm = (2/np.sqrt(1+delta)) * ((m+1)/np.sqrt((l-m)*(l+m+1)))
            b_lm = (1 / np.sqrt(1 + delta)) * ((np.sqrt((l + m + 2) * (l - m - 1))) / (np.sqrt((l - m) * (l + m + 1))))
            p_lm[l, m] = a_lm * (np.cos(theta)/u) * p_lm[l, m + 1] - b_lm * p_lm[l, m+2]
    return p_lm

# function to read the Stokes coefficients
def readstrokescoefficients(filename):
    with open(filename) as f:
        reach_end_of_head = 0
        for line in f:
            line = line.strip()
            if reach_end_of_head == 0:
                if line.startswith("earth_gravity_constant"):
                    GM = float(line[line.index(" ") + 1:])
                elif "radius" in line:
                    line = re.sub(' +', ' ', line)
                    R = float(line.split(" ")[1])
                elif line.startswith("max_degree"):
                    line = re.sub(' +', ' ', line)
                    max_degree = int(line.split(" ")[1])
                    C = np.zeros((max_degree + 1, max_degree + 1))
                    S = np.zeros((max_degree + 1, max_degree + 1))
                else:
                    if line.startswith("end_of_head"):
                        reach_end_of_head = 1
            else:
                line = re.sub(' +', ' ', line)
                line = line.split()
                L = int(line[1])
                M = int(line[2])
                C[L, M] = float(line[3])
                S[L, M] = float(line[4])
    return C, S, R, GM


print ('Reading input data')

# load Love numbers if needed
#k_l = np.loadtxt("loadLoveNumbers_96.txt")
#C_2003, S_2003, R, GM = readstrokescoefficients("Input/ITSG-Grace2018_n96_2003-07.gfc")
#C_2013, S_2013, R, GM = readstrokescoefficients("Input/ITSG-Grace2018_n96_2013-07.gfc")
C_2003, S_2003, R, GM = readstrokescoefficients("Input/CSR_CSR-Release-06_60x60_DDK4_kfilter_DDK4_GSM-2_2003182-2003212_GRAC_UTCSR_BA01_0600.gfc")
C_2013, S_2013, R, GM = readstrokescoefficients("Input/CSR_CSR-Release-06_60x60_DDK4_kfilter_DDK4_GSM-2_2013182-2013212_GRAC_UTCSR_BA01_0600.gfc")

# Compute the difference between the two solutions by subtracting
# the two sets of Stokes coefficients from each other
dclm_2003 = C_2003 - ((C_2003 + C_2013) / 2)
dslm_2003 = S_2003 - ((S_2003 + S_2013) / 2)
dclm_2013 = C_2013 - ((C_2013 + C_2003) / 2)
dslm_2013 = S_2013 - ((S_2013 + S_2003) / 2)

# define necessary constants
PavPw = 5.5
l = 60
m = 60

print ('Computation of Fourier coefficients')

# convert the resulting Stokes coefficients into the Fourier coefficients of gravitational potential.
C_dhw_2003 = np.zeros((l+1, l+1))
S_dhw_2003 = np.zeros((l+1, l+1))
C_dhw_2013 = np.zeros((l+1, l+1))
S_dhw_2013 = np.zeros((l+1, l+1))
for i in range(l+1):
    for j in range(i+1):
#       multiplication_factor = PavPw * (R * (2 * i + 1)) / (3 * (1 + k_l[i, 1]))
#       multiplication_factor = GM/R
        multiplication_factor = GM/R*(R/(R+500000))**(i+1)
        C_dhw_2003[i, j] = (dclm_2003[i, j] * multiplication_factor)
        S_dhw_2003[i, j] = (dslm_2003[i, j] * multiplication_factor)
        C_dhw_2013[i, j] = (dclm_2013[i, j] * multiplication_factor)
        S_dhw_2013[i, j] = (dslm_2013[i, j] * multiplication_factor)

# define grid
#_lambda = np.pi/180 * np.array(range(359 + 1))
#theta = np.pi/180 * np.array(range(1, 179 + 1))
_lambda = np.pi/180 * np.array(range(0,359 + 1,4))
theta = np.pi/180 * np.array(range(2, 179 + 1,4))
print ('_lambda=',_lambda/np.pi*180)
print ('theta=',theta/np.pi*180)

# initialize the output grid
ewh2003 = np.zeros((len(theta), len(_lambda)))
ewh2013 = np.zeros((len(theta), len(_lambda)))

print ('Start of the computation of EWH at the grid nodes')

# computational algorithm
for i in range(len(theta)):                              # loop over all thetas
    print ('i=',i,' (out of',len(theta),')')
    P_lm = plm(theta[i], l)                              # all Legendre Functions for one theta
    for j in range(len(_lambda)):                        # loop over all lambdas
        for k in range(l+1):                             # loop over all degrees
            for t in range(k+1):                         # loop over negative orders
                sin_t_lambda = np.sin(t*_lambda[j])      # negative orders
                cos_t_lambda = np.cos(t*_lambda[j])      # non-negative orders
                # compute here equivalent water heights
                ewh2003[i, j] = ewh2003[i, j] + (S_dhw_2003[k, t] * P_lm[k, t] * sin_t_lambda)
                ewh2013[i, j] = ewh2013[i, j] + (S_dhw_2013[k, t] * P_lm[k, t] * sin_t_lambda)
                ewh2003[i, j] = ewh2003[i, j] + (C_dhw_2003[k, t] * P_lm[k, t] * cos_t_lambda)
                ewh2013[i, j] = ewh2013[i, j] + (C_dhw_2013[k, t] * P_lm[k, t] * cos_t_lambda)

# make plot

# global plot

latitude = np.pi/2 - theta
# MAP = Basemap(projection='cyl', llcrnrlon=0., urcrnrlon=360., resolution='c', llcrnrlat=-90., urcrnrlat=90.)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())

# draw coastlines
# MAP.drawcoastlines()
ax.coastlines()

x, y = np.meshgrid(np.rad2deg(_lambda), np.rad2deg(latitude))

# make the plot
cnplot = ax.pcolormesh(x, y, ewh2013-ewh2003)

# make  the color bar
clevs = np.arange(-3, 3, 1)
cbar = fig.colorbar(cnplot, location='bottom')

plt.xlabel('Longitude', labelpad=8, fontsize=8)
plt.ylabel('Latitude', labelpad=8, fontsize=8)
plt.title('Change in gravitational potential at H=500 km in 2003-2013 (m^2/s^2)', pad=14, fontsize=12)
plt.savefig("V_altitude-500.pdf")
#plt.savefig("V_altitude-500.eps")
plt.show()
 
