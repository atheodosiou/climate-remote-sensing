#!/usr/bin/env python3
from pathlib import Path

import numpy as np
import scipy as sp
import pymap3d as pm
from scipy.constants import G
from pyproj import Geod
from shapely.geometry import Polygon

import geometry
from gravity_disturbance import calc_grav_disturbance, setup_grav_computation

def read_polygons(path_to_polygons):
    polygons = []
    for polygon_fn in path_to_polygons.glob("./*.txt"):
        lons_lats = np.loadtxt(polygon_fn)
        polygons.append(lons_lats)
    return polygons

def get_areas(lats, lons):
    squares = get_lat_squares(lats, lons)
    geod = Geod(ellps="WGS84")
    areas = []
    for square in squares:
        area, _ = geod.geometry_area_perimeter(square)
        areas.append(area)
    return np.array(areas)


def get_lat_squares(lats, lons):
    """Construct shapely Polygons for a single longitude but
    every latitude.
    Area is constant with longitude so just copy results.
    """
    lats_0 = lats[:-1]
    lats_1 = lats[1:]
    lons_0 = lons[0:1]
    lons_1 = lons[1:2]

    c1 = stack_coords(lons_0, lats_0)
    c2 = stack_coords(lons_1, lats_0)
    c3 = stack_coords(lons_1, lats_1)
    c4 = stack_coords(lons_0, lats_1)

    squares = []
    for p1, p2, p3, p4 in zip(c1, c2, c3, c4):
        squares.append(Polygon([p1, p2, p3, p4]))
    return squares


def stack_coords(x, y):
    """Stacks coordinate lists into a 2D array of coordinate pairs,
    flattened to list of coordinate pairs"""
    out = np.vstack(np.stack(np.meshgrid(x, y), axis=2))
    return out


def node_area(polygon, valid_points):
    geod = Geod(ellps="WGS84")
    area, _ = geod.geometry_area_perimeter(Polygon(polygon))
    area = np.abs(area)
    nr_valid_points = np.sum(valid_points)
    area_per_node = area / nr_valid_points
    return area_per_node


def invert():

# PD: Start
    print("Start of computations")

    lon, lat = geometry.gaussian_grid(1, -180, 180)
    # Geocentric coordinates at 500 km above the surface
    lon_vv, lat_vv = np.meshgrid(lon, lat)
    phi_p_vv, lambda_p_vv, r_p_vv = pm.spherical.geodetic2spherical(lat_vv, lon_vv, 500e3)
    # Geocentric coordinates on the surface
    phi_s_vv, lambda_s_vv, r_s_vv = pm.spherical.geodetic2spherical(lat_vv, lon_vv, 0)
    # Get areas of grid cells on the surface
    # areas = np.zeros((lat.shape[0] - 1, lon.shape[0] - 1))
    # for i in range(lon.shape[0] - 1):
    #     areas[:, i] = get_areas(phi_s, lambda_s[i : i + 2])
    path_to_polygons = Path("../Input/Jiangjuns_polygons/23_mascons/orig/")
    polygons = read_polygons(path_to_polygons)
    masks = list(map(lambda polygon : geometry.point_in_polygon(lambda_s_vv, phi_s_vv, polygon), polygons))
    # We assume that the area of each node within a mascon is equal to
    # the average node of that mascon
    area_per_node = list(map(node_area, polygons, masks))
    dclm_2003, dclm_2013, dslm_2003, dslm_2013, R, GM = setup_grav_computation()
    love_numbers_file = Path("../Input/load_love_numbers_96.txt")
    k_l = np.loadtxt(love_numbers_file, skiprows=0, usecols=1)
    path_to_obs_polygons_mat = Path("../Input/Jiangjuns_polygon_entire_Greenland/poly_cell_obs_300km.mat")
    obs_poly_mat = sp.io.loadmat(path_to_obs_polygons_mat)
    obs_poly_300 = obs_poly_mat["poly_cell_obs"][0,0].astype(float)
    # lambda_p_vv, phi_p_vv = np.meshgrid(lambda_p, phi_p)
    theta_p_vv = 90 - phi_p_vv
    obs_mask = geometry.point_in_polygon(lambda_p_vv, phi_p_vv, obs_poly_300)
    g_2003, g_2013 = calc_grav_disturbance(dclm_2003, dclm_2013, dslm_2003, dslm_2013, k_l, np.deg2rad(theta_p_vv[obs_mask]), np.deg2rad(lambda_p_vv[obs_mask]), R, GM, grid=False)
    dg = g_2013 - g_2003
    A = np.zeros((dg.size, len(polygons)))
    for i, j in np.ndindex(A.shape):
        # ith point and jth mascon
        mascon_mask = masks[j]
        r_p_i = r_p_vv[obs_mask][i]
        r_s_j = r_s_vv[mascon_mask]
        lambda_p_i = lambda_p_vv[obs_mask][i]
        lambda_s_j = lambda_s_vv[mascon_mask]
        phi_p_i = phi_p_vv[obs_mask][i]
        phi_s_j = phi_s_vv[mascon_mask]
        distance, cos_psi = geometry.spherical_distance(r_p_i, r_s_j, lambda_p_i, lambda_s_j, phi_p_i, phi_s_j)
        numerator = area_per_node[j] * (r_p_i - r_s_j * cos_psi)
        denominator = distance ** 3
        A[i, j] = G * np.sum(numerator / denominator)
    # The following asks numpy to find the least-squares solution
    # rho, residuals, rank, singular_values = np.linalg.lstsq(A, dg)
    # Instead we're explicitly defining the normal equation
    N = A.T @ A
    b = A.T @ dg
    rho = np.linalg.solve(N, b)
    print("Least-squares solution for the surface densities:")
    print(f"{rho=}")
    # print(f"{residuals=}")
    # print(f"{rank=}")
    # print(f"{singular_values=}")

# PD: conversion into the mass anomalies (in Gt) is added
    def mascon_area(polygon):
        geod = Geod(ellps="WGS84")
        area, _ = geod.geometry_area_perimeter(Polygon(polygon))
        return area
    mascon_areas = np.zeros((rho.size))
    mass_anomalies = np.zeros((rho.size))
    mass_anomaly_total = 0
    for i in range(rho.size):
        area=mascon_area(polygons[i])
        area = np.abs(area)
        mascon_areas[i] = area
        mass_anomalies[i] = rho[i]*area*1e-12
        mass_anomaly_total = mass_anomaly_total+mass_anomalies[i]
    print(f"{mascon_areas=}")
    print(f"{mass_anomalies=}")
    print(f"{mass_anomaly_total=}")
    return rho, mass_anomalies, polygons

if __name__ == "__main__":
    invert()
