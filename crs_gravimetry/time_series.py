import re
from pprint import pprint
from pathlib import Path

import numpy as np
import scipy as sp
import pymap3d as pm
from scipy.constants import G
from scipy import stats

import geometry
import inversion


def range_of_files(directory, min_year, max_year):
    """Find the files in a directory whose filenames have numbers that fall
    within the range specified by min_year and max_year.


    Parameters
    ----------
    directory : str | os.PathLike
        The path to the directory that contains the files to be checked.
    min_year : int
        The minimum year.
    max_year : int
        The maximum year.

    Returns
    -------
    files : list
        The list of files that fall within the range of years.
    """
    files = []
    for f in directory.iterdir():
        if match := re.search(r"\d+", f.name):
            year = int(match.group())
            if year >= min_year and year <= max_year:
                files.append(f)
    return files


def resample_time_series(min_year, max_year):
    """Resample the global mass-anomaly time series to the 23 mascons over
    Greenland.

    The value of each mascon is the mean of all the points in the grid that fall
    within it.


    Parameters
    ----------
    min_year : int
        The minimum year to use when constructing the time series.
    max_year : int
        The maximum year to use when constructing the time series.

    Returns
    -------
    mass_anomalies_series : ndarray
        An [N-D] time series of mass anomalies. N is the number of points in the
        time series and D the number of mascons.
    """
    path_to_mask = Path(
        "../Input/monthly-grids-of-global-mass-anomalies_2023-09-29_0937/global_0.4x1d_no-small-6x2_sep-Gr.ind"
    )
    ma_grid_mask = np.loadtxt(path_to_mask, skiprows=1)
    # In the text file, the entries with value 3 are the ones over Greenland
    ma_grid_mask = ma_grid_mask[:, 2] == 3
    path_to_polygons = Path("../Input/Jiangjuns_polygons/23_mascons/orig/")
    polygons = inversion.read_polygons(path_to_polygons)
    path_to_mass_anomalies = Path(
        "../Input/monthly-grids-of-global-mass-anomalies_2023-09-29_0937/Global_EWH_grids"
    )
    ma_files = range_of_files(path_to_mass_anomalies, 2004, 2008)
    lons = np.loadtxt(ma_files[0], skiprows=1, usecols=0)
    lats = np.loadtxt(ma_files[0], skiprows=1, usecols=1)
    lons = lons[ma_grid_mask]
    lons_180 = (lons + 180) % 360 - 180
    lats = lats[ma_grid_mask]
    mass_anomalies_series = np.zeros((len(ma_files), len(polygons)))
    for i in range(len(ma_files)):
        mass_anomalies = np.loadtxt(ma_files[i], skiprows=1, usecols=2)
        mass_anomalies = mass_anomalies[ma_grid_mask]
        mascon_masks = list(
            map(
                lambda polygon: geometry.point_in_polygon(lons_180, lats, polygon),
                polygons,
            )
        )
        mascon_mass_anomalies = list(
            map(
                lambda mascon_mask: np.mean(mass_anomalies, where=mascon_mask),
                mascon_masks,
            )
        )
        mascon_mass_anomalies = np.array(mascon_mass_anomalies)
        mass_anomalies_series[i] = mascon_mass_anomalies
    return lons_180, lats, mass_anomalies_series


def forward(rho_anomaly, ma_lon, ma_lat, lambda_dg, phi_dg, r_dg, path_to_mascons):
    """Run the forward calculation.

    Given a set of mass anomalies calculate the gravity disturbances.


    Parameters
    ----------
    rho_anomaly : ndarray
        The surface density anomalies in units of kg/m^2.
    ma_lon : ndarray
        The longitudes of the surface density anomalies.
    ma_lat : ndarray
        The latitudes of the surface density anomalies.
    obs_polygon : shapely.Polygon
        The polygon defining the area over which the gravity disturbances will be calculated.
    k_l : ndarray
        Load Love numbers.


    Returns
    -------
    dg : ndarray
        The gravity disturbances.
    """
    # Geocentric coordinates at the surface
    phi_s, lambda_s, r_s = pm.spherical.geodetic2spherical(ma_lat, ma_lon, 0)
    # read the mascon polygons
    mascon_polygons = inversion.read_polygons(path_to_mascons)
    mascon_masks = list(
        map(
            lambda polygon: geometry.point_in_polygon(lambda_s, phi_s, polygon),
            mascon_polygons,
        )
    )
    # Geocentric coordinates at 500 km above the surface
    # lon_vv, lat_vv = np.meshgrid(lon_dg, lat_dg)
    # phi_p_vv, lambda_p_vv, r_p_vv = pm.spherical.geodetic2spherical(
    #     lat_vv, lon_vv, 500e3
    # )
    # obs_mask = geometry.point_in_polygon(lambda_p_vv, phi_p_vv, obs_polygon)
    area_per_node = list(map(inversion.node_area, mascon_polygons, mascon_masks))
    A = np.zeros((lat_dg.size, len(mascon_masks)))
    for i, j in np.ndindex(A.shape):
        # ith point and jth mascon
        mascon_mask = mascon_masks[j]
        r_p_i = r_dg[i]
        r_s_j = r_s[mascon_mask]
        lambda_p_i = lambda_dg[i]
        lambda_s_j = lambda_s[mascon_mask]
        phi_p_i = phi_dg[i]
        phi_s_j = phi_s[mascon_mask]
        distance, cos_psi = geometry.spherical_distance(
            r_p_i, r_s_j, lambda_p_i, lambda_s_j, phi_p_i, phi_s_j
        )
        numerator = area_per_node[j] * (r_p_i - r_s_j * cos_psi)
        denominator = distance**3
        A[i, j] = G * np.sum(numerator / denominator)
    dg = A @ rho_anomaly
    return dg

def grav_disturbance_time_series(lon_dg, lat_dg, obs_polygon, lon_rho, lat_rho, rho, path_to_mascon_polygons):
    """Construct the gravitational disturbance time series from a surface density mass series.


    Parameters
    ----------
    lon : ndarray
        The longitudes that will form the grid.
    lat : ndarray
        The latitudes that will form the grid.
    obs_polygon : ndarray
        The region over which to compute the gravity disturbances.
    rho : ndarray
        The surface-density anomaly time series.

    Returns
    -------
    dg : ndarray
        The gravity disturbance time series.
    """
    lon_vv, lat_vv = np.meshgrid(lon_dg, lat_dg)
    phi_p_vv, lambda_p_vv, r_p_vv = pm.spherical.geodetic2spherical(
        lat_vv, lon_vv, 500e3
    )
    obs_mask = geometry.point_in_polygon(lambda_p_vv, phi_p_vv, obs_polygon)
    phi_p, lambda_p, r_p = phi_p_vv[obs_mask], lambda_p_vv[obs_mask], r_p_vv[obs_mask]
    dg = np.zeros((rho.shape[0], np.sum(obs_mask)))
    for i in range(rho.shape[0]):
         dg[i] = forward(rho[i], lon_rho, lat_rho, phi_p, lambda_p, r_p, path_to_mascon_polygons)
    return dg

def fit_linear_model_per_mascon(rho_time_series):
    """Fit a linear regression model to each mascon of the surface density time series


    Parameters
    ----------
    rho_time_series : ndarray
        The surface density time series

    Returns
    -------
    slope, intercept : tuple
        The slope and intercept of the linear model for each mascon
    """
    time = np.arange(rho_time_series.shape[0])
    slope, intercept = np.zeros(rho_time_series.shape[1]), np.zeros(rho_time_series.shape[1])
    for i in range(rho_time_series.shape[1]):
        res = stats.linregress(time, rho_time_series[:, i])
        slope[i] = res.slope
        intercept[i] = res.intercept
    return slope, intercept


if __name__ == "__main__":
    lon_rho, lat_rho, mass_anomaly_series_ewh = resample_time_series(2004, 2008)
    mass_anomaly_series_ewh -= mass_anomaly_series_ewh[0, :]
    rho_time_series = mass_anomaly_series_ewh * 997.048
    path_to_obs_polygons_mat = Path("../Input/Jiangjuns_polygon_entire_Greenland/poly_cell_obs_300km.mat")
    obs_poly_mat = sp.io.loadmat(path_to_obs_polygons_mat)
    obs_poly_300 = obs_poly_mat["poly_cell_obs"][0,0].astype(float)
    path_to_mascon_polygons = Path("../Input/Jiangjuns_polygons/23_mascons/orig/")
    dg_time_series = np.zeros((rho_time_series.shape[0], ))
    lon_dg, lat_dg = geometry.gaussian_grid(2, -75, -10)
    dg = grav_disturbance_time_series(lon_dg, lat_dg, obs_poly_300, lon_rho, lat_rho, rho_time_series, path_to_mascon_polygons)
