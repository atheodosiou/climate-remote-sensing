import re
from pathlib import Path

import numpy as np
import numpy.ma as ma
import scipy as sp
import pymap3d as pm
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import cartopy.crs as ccrs

import geometry

def setup_grav_computation():
    print ('Reading input data')
    C_2003, S_2003, _, _ = readstrokescoefficients("../Input/CSR_CSR-Release-06_60x60_DDK4_kfilter_DDK4_GSM-2_2003182-2003212_GRAC_UTCSR_BA01_0600.gfc")
    C_2013, S_2013, R, GM = readstrokescoefficients("../Input/CSR_CSR-Release-06_60x60_DDK4_kfilter_DDK4_GSM-2_2013182-2013212_GRAC_UTCSR_BA01_0600.gfc")
    # Compute the difference between the two solutions by subtracting
    # the two sets of Stokes coefficients from each other
    dclm_2003 = C_2003 - ((C_2003 + C_2013) / 2)
    dslm_2003 = S_2003 - ((S_2003 + S_2013) / 2)
    dclm_2013 = C_2013 - ((C_2013 + C_2003) / 2)
    dslm_2013 = S_2013 - ((S_2013 + S_2003) / 2)

    return dclm_2003, dclm_2013, dslm_2003, dslm_2013, R, GM

def plm(theta, degree):
    # function to compute normalized Legendre function
    p_lm = np.zeros((degree + 1, degree + 1))
    u = np.sqrt(1 - np.cos(theta)**2)
    for l in range(degree +1):
        if l == 0:
            p_lm[l, l] = 1
        elif l == 1:
            p_lm[l, l] = u * np.sqrt(3)
        elif l > 1:
            p_lm[l, l] = u * np.sqrt((2*l + 1) / (2*l)) * p_lm[l-1, l-1]

    for l in range(degree + 1):
        for m in range(0, l+1):
            if m == l-1:
                if m == 0:
                    delta = 1
                else:
                    delta = 0
                a_lm = (2/np.sqrt(1+delta)) * ((m+1)/np.sqrt((l-m)*(l+m+1)))
                p_lm[l, m] = a_lm * (np.cos(theta)/u) * p_lm[l, m+1]

    for l in range(degree + 1):
        for m in range(l-2, 0 - 1, -1):
            if m == 0:
                delta = 1
            else:
                delta = 0
            a_lm = (2/np.sqrt(1+delta)) * ((m+1)/np.sqrt((l-m)*(l+m+1)))
            b_lm = (1 / np.sqrt(1 + delta)) * ((np.sqrt((l + m + 2) * (l - m - 1))) / (np.sqrt((l - m) * (l + m + 1))))
            p_lm[l, m] = a_lm * (np.cos(theta)/u) * p_lm[l, m + 1] - b_lm * p_lm[l, m+2]
    return p_lm

# function to read the Stokes coefficients
def readstrokescoefficients(filename):
    with open(filename) as f:
        reach_end_of_head = 0
        for line in f:
            line = line.strip()
            if reach_end_of_head == 0:
                if line.startswith("earth_gravity_constant"):
                    GM = float(line[line.index(" ") + 1:])
                elif "radius" in line:
                    line = re.sub(' +', ' ', line)
                    R = float(line.split(" ")[1])
                elif line.startswith("max_degree"):
                    line = re.sub(' +', ' ', line)
                    max_degree = int(line.split(" ")[1])
                    C = np.zeros((max_degree + 1, max_degree + 1))
                    S = np.zeros((max_degree + 1, max_degree + 1))
                else:
                    if line.startswith("end_of_head"):
                        reach_end_of_head = 1
            else:
                line = re.sub(' +', ' ', line)
                line = line.split()
                L = int(line[1])
                M = int(line[2])
                C[L, M] = float(line[3])
                S[L, M] = float(line[4])
    return C, S, R, GM

def calc_grav_disturbance(dclm_t1, dclm_t2, dslm_t1, dslm_t2, k_l, theta_p, lambda_p, R, GM, grid=True):
    # define necessary constants
    PavPw = 5.5
    l = 60
    m = 60

    print ('Computation of Fourier coefficients')

    # convert the resulting Stokes coefficients into the Fourier coefficients of gravitational potential.
    C_dhw_t1 = np.zeros((l+1, l+1))
    S_dhw_t1 = np.zeros((l+1, l+1))
    C_dhw_t2 = np.zeros((l+1, l+1))
    S_dhw_t2 = np.zeros((l+1, l+1))
    r_p = R + 500E3
    GM_over_rp_sq = GM / r_p**2
    R_over_rp = R / r_p
    for i in range(l+1):
        for j in range(i+1):
            multiplication_factor = GM_over_rp_sq * ((i + 2) / (k_l[i] + 1)) * R_over_rp**(i+1)
            C_dhw_t1[i, j] = (dclm_t1[i, j] * multiplication_factor)
            S_dhw_t1[i, j] = (dslm_t1[i, j] * multiplication_factor)
            C_dhw_t2[i, j] = (dclm_t2[i, j] * multiplication_factor)
            S_dhw_t2[i, j] = (dslm_t2[i, j] * multiplication_factor)

    print ('_lambda=',lambda_p/np.pi*180)
    print ('theta=',theta_p/np.pi*180)
    print ('Start of the computation of gravity disturbance at the grid nodes.')

    # computational algorithm
    # if the lat and lon form a grid
    if grid:
            # initialize the output grid
        g_t1 = np.zeros((len(theta_p), len(lambda_p)))
        g_t2 = np.zeros((len(theta_p), len(lambda_p)))
        for i in range(len(theta_p)):                              # loop over all thetas
            print ('i=',i,' (out of',len(theta_p),')')
            P_lm = plm(theta_p[i], l)                              # all Legendre Functions for one theta
            for j in range(len(lambda_p)):                        # loop over all lambdas
                for k in range(l+1):                             # loop over all degrees
                    for t in range(k+1):                         # loop over negative orders
                        sin_t_lambda = np.sin(t*lambda_p[j])      # negative orders
                        cos_t_lambda = np.cos(t*lambda_p[j])      # non-negative orders
                        # compute here equivalent water heights
                        g_t1[i, j] = g_t1[i, j] + (S_dhw_t1[k, t] * P_lm[k, t] * sin_t_lambda)
                        g_t2[i, j] = g_t2[i, j] + (S_dhw_t2[k, t] * P_lm[k, t] * sin_t_lambda)
                        g_t1[i, j] = g_t1[i, j] + (C_dhw_t1[k, t] * P_lm[k, t] * cos_t_lambda)
                        g_t2[i, j] = g_t2[i, j] + (C_dhw_t2[k, t] * P_lm[k, t] * cos_t_lambda)
    # if the lat and lon are actually pairs of coordinates
    else:
        g_t1 = np.zeros_like(theta_p)
        g_t2 = np.zeros_like(theta_p)
        for i in range(len(theta_p)):                              # loop over all thetas
            # print ('i=',i,' (out of',len(theta_p),')')
            P_lm = plm(theta_p[i], l)                              # all Legendre Functions for one theta
            for k in range(l+1):                             # loop over all degrees
                for t in range(k+1):                         # loop over negative orders
                    sin_t_lambda = np.sin(t*lambda_p[i])      # negative orders
                    cos_t_lambda = np.cos(t*lambda_p[i])      # non-negative orders
                    # compute here equivalent water heights
                    g_t1[i] = g_t1[i] + (S_dhw_t1[k, t] * P_lm[k, t] * sin_t_lambda)
                    g_t2[i] = g_t2[i] + (S_dhw_t2[k, t] * P_lm[k, t] * sin_t_lambda)
                    g_t1[i] = g_t1[i] + (C_dhw_t1[k, t] * P_lm[k, t] * cos_t_lambda)
                    g_t2[i] = g_t2[i] + (C_dhw_t2[k, t] * P_lm[k, t] * cos_t_lambda)
    return g_t1, g_t2

if __name__ == '__main__':
    lon, lat = geometry.gaussian_grid(1, -180, 180)
    # Geocentric coordinates at 500 km above the surface
    lon_vv, lat_vv = np.meshgrid(lon, lat)
    phi_p_vv, lambda_p_vv, r_p_vv = pm.spherical.geodetic2spherical(lat_vv, lon_vv, 500e3)
    dclm_2003, dclm_2013, dslm_2003, dslm_2013, R, GM = setup_grav_computation()
    love_numbers_file = Path("../Input/load_love_numbers_96.txt")
    k_l = np.loadtxt(love_numbers_file, skiprows=0, usecols=1)
    path_to_obs_polygons_mat = Path("../Input/Jiangjuns_polygon_entire_Greenland/poly_cell_obs_300km.mat")
    obs_poly_mat = sp.io.loadmat(path_to_obs_polygons_mat)
    obs_poly_300 = obs_poly_mat["poly_cell_obs"][0,0].astype(float)
    theta_p_vv = 90 - phi_p_vv
    obs_mask = geometry.point_in_polygon(lambda_p_vv, phi_p_vv, obs_poly_300)
    g_2003, g_2013 = calc_grav_disturbance(dclm_2003, dclm_2013, dslm_2003, dslm_2013, k_l, np.deg2rad(theta_p_vv[obs_mask]), np.deg2rad(lambda_p_vv[obs_mask]), R, GM, grid=False)
    dg = g_2013 - g_2003
    dg_vv = ma.masked_all_like(lon_vv)
    dg_vv[obs_mask] = dg
    ma.set_fill_value(dg_vv, np.nan)
    fig = plt.figure(figsize=(10,5))
    bounds = [-86.24487804656432, 6.439721176407588, 57.132352624307536, 86.34796469012537]
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.LambertConformal((bounds[0] + bounds[1])/2, (bounds[2] + bounds[3])/2, cutoff=0))
    ax.set_extent([-87, 7, 50, 88])
    filled_c = ax.contourf(lon_vv, lat_vv, dg_vv, transform=ccrs.PlateCarree(), cmap="viridis")
    ax.coastlines()
    cbar = fig.colorbar(filled_c, orientation='vertical')
    plt.show()
