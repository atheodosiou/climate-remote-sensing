#!/usr/bin/env python3
from pathlib import Path

import numpy as np
import numpy.ma as ma
import pymap3d as pm
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.colors as colors
import cartopy.crs as ccrs
from shapely import Polygon

import geometry
import inversion

# PD: the code is adjusted to deal with arbitrary quantities
# def plot_surface_disturbances(lons, lats, rho, polygons, save_path):
def plot_surface_disturbances(lons, lats, rho, polygons, save_path, quantity):

    fig = plt.figure(figsize=(10,5))
    bounds = [-86.24487804656432, 6.439721176407588, 57.132352624307536, 86.34796469012537]
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.LambertConformal((bounds[0] + bounds[1])/2, (bounds[2] + bounds[3])/2, cutoff=0))
    filled_c = ax.contourf(lons, lats, rho, transform=ccrs.PlateCarree(), norm=colors.CenteredNorm(0), cmap="coolwarm")
    xlim = [bounds[0]-1, bounds[1]+1]
    ylim = [57, 90]
    lower_space = 10
    rect = mpath.Path([[xlim[0], ylim[0]],
                       [xlim[1], ylim[0]],
                       [xlim[1], ylim[1]],
                       [xlim[0], ylim[1]],
                       [xlim[0], ylim[0]],
                       ]).interpolated(20)

    proj_to_data = ccrs.PlateCarree()._as_mpl_transform(ax) - ax.transData
    rect_in_target = proj_to_data.transform_path(rect)

    ax.set_boundary(rect_in_target)
    ax.set_extent([xlim[0], xlim[1], ylim[0] - lower_space, ylim[1]])
    ax.coastlines()
    ax.add_geometries([Polygon(p) for p in polygons], crs=ccrs.PlateCarree(), facecolor="none", lw=0.5)
    cbar = fig.colorbar(filled_c, orientation='vertical')

    # PD: the code is adjusted to deal with arbitrary quantities
    cbar.ax.set_ylabel(quantity)
    ax.set_title("Retrieved "+quantity+" over Greenland")
    fig.savefig(save_path, dpi=300)


if __name__ == '__main__':
    # PD: mass anomalies are imported as well
    rho, mass_anomalies, mascon_polygons = inversion.invert()
    # let's create a fine grid for the contour plot
    lon, lat = geometry.gaussian_grid(0.25, -180, 180)
    lon_vv, lat_vv = np.meshgrid(lon, lat)
    phi_s_vv, lambda_s_vv, r_s_vv = pm.spherical.geodetic2spherical(lat_vv, lon_vv, 0)
    masks = list(map(lambda polygon : geometry.point_in_polygon(lambda_s_vv, phi_s_vv, polygon), mascon_polygons))
    rho_vv = ma.masked_all_like(phi_s_vv)
    for r, m in zip(rho, masks):
        rho_vv[m] = r
    ma.set_fill_value(rho_vv, np.nan)

    # PD: the code is adjusted to deal with aritrary quantities
    save_path = Path("../Output/surface_densities.png")
    quantity = "surface densities"
    plot_surface_disturbances(lon_vv, lat_vv, rho_vv, mascon_polygons, save_path, quantity)

    # PD: plotting mass anomalies is added
    mass_anomalies_vv = ma.masked_all_like(phi_s_vv)
    for r, m in zip(mass_anomalies, masks):
        mass_anomalies_vv[m] = r
    ma.set_fill_value(mass_anomalies_vv, np.nan)
    save_path = Path("../Output/mass_anomalies.png")
    quantity = "mass anomalies"
    plot_surface_disturbances(lon_vv, lat_vv, mass_anomalies_vv, mascon_polygons, save_path, quantity)
    plt.show()
