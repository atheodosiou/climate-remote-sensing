import numpy as np
import shapely
try:
    from pygeodesy.ellipsoidalNvector import LatLon
except ImportError:
    _has_pygeodesy = False
else:
    _has_pygeodesy = True


def point_in_polygon(lon, lat, polygon):
    """Check if the point(s) defined by the longitude and latitude
    lies withing the polygon.


    Parameters
    ----------
    lon : float | ndarray
        The longitude or an array of longitudes of the point(s) to be
        checked.
    lat: float | ndarray
        The latitude or an array of latitudes of the point(s) to be
        checked.
    polygon : np.ndarray
       The polygon that defines the area that the points will be
       checked against.

    Returns
    -------
    out : bool | ndarray
        True if the point lies in the polygon. If an array of points
        is given, a mask array is provided as an output.
    """
    points = shapely.points(lon, lat)
    polygon = shapely.Polygon(polygon)
    return shapely.contains(polygon, points)

def point_in_polygon_geo(lon, lat, polygon):
    """Check if the point(s) defined by the longitude and latitude
    lies withing the polygon. The function does not assume that the
    point and polygon lie in the same plane. This function requires
    the pygeodesy library to be available.


    Parameters
    ----------
    lon : float | ndarray
        The longitude or an array of longitudes of the point(s) to be
        checked.
    lat: float | ndarray
        The latitude or an array of latitudes of the point(s) to be
        checked.
    polygon : ndarray
        The polygon that defines the area that the points will be
        checked against.

    Returns
    -------
    out : bool | ndarray
        True if the point lies in the polygon. If an array of points
        is given, a mask array is provided as an output.
    """
    if not _has_pygeodesy:
        raise ImportError("pygeodesy is required for this function to work.")
    polygon = [LatLon(y, x) for x,y in polygon]
    lon, lat = np.atleast_1d(lon), np.atleast_1d(lat)
    mask = np.zeros_like(lon, dtype=bool)
    it = np.nditer([lon, lat], flags=["multi_index"])
    for x, y in it:
        p = LatLon(y, x)
        mask[it.multi_index] = p.isenclosedBy(polygon)
    return np.squeeze(mask)


def gaussian_grid(res, min_lon=-180, max_lon=180, min_lat=-90, max_lat=90):
    """Define a Gauss-Neumann grid in terms of latitude and longitude.

    This function generates a grid of latitude and longitude points, based on a
    specified resolution.

    Parameters
    ----------
    res : float
        The resolution of the grid.

    Returns:
    -------
    lat : ndarray
        1D array containing latitude values in degrees for each grid cell.
    lon : ndarray
        1D array containing longitude values in degrees for each grid cell.
    """
    l = int((max_lat - min_lat) // res)
    n = l - 1

    # longitudes
    dl = 180 / n
    lon = np.arange(min_lon, max_lon, dl)

    # co-latitudes
    p, _ = np.polynomial.legendre.leggauss(l)
    lat = 90 - np.rad2deg(np.arccos(-p))  # 90 - co-lat = lat

    return lon, lat

def spherical_distance(r_p, r_s, lambda_p, lambda_s, phi_p, phi_s):
    """Calculate the distance between two points in spherical coordinates.


    Parameters
    ----------
    r_p : float
        The radial coordinate of the 1st point [m].
    r_s : float
        The radial coordinate of the 2nd point [m].
    lambda_p : float
        The longitude of the 1st point [deg].
    lambda_s : float
        The longitude of the 2nd point [deg].
    phi_p : float
        The latitude of the 1st point [deg].
    phi_s : float
        The latitude of the 2nd point [deg].

    Returns
    -------
    out : float
       The distance between the two points [m].
    """
    lambda_p_r, lambda_s_r = np.deg2rad(lambda_p), np.deg2rad(lambda_s)
    phi_p_r, phi_s_r = np.deg2rad(phi_p), np.deg2rad(phi_s)
    cos_psi = np.sin(phi_p_r) * np.sin(phi_s_r) + np.cos(phi_p_r) * np.cos(phi_s_r) * np.cos(lambda_p_r - lambda_s_r)
    distance_sq = r_s ** 2 + r_p ** 2 - 2 * r_s * r_p * cos_psi
    distance = np.sqrt(distance_sq)
    return distance, cos_psi
    
